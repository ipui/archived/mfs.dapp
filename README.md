# ipui mfs dapp

manage your ipfs mutable file system

this web dapp is developed to work with mutable file system [msf](https://github.com/ipfs/interface-js-ipfs-core/blob/master/SPEC/FILES.md#the-files-api-aka-mfs-the-mutable-file-system) of [ipfs](https://ipfs.io).

**this is not an implementation of an entire ipfs node**, this project only use the `api` that the _daemon_ binds over the port `5001` (by default). [go](https://github.com/ipfs/go-ipfs) or [javascript](https://github.com/ipfs/js-ipfs) implementation is by your own.

## ip[fn]s links

* [/ipns/12D3KooWCcbggDkc8B25u51XktYSBv1b8g9W31Zii5ovyJPHJ86X](https://gateway.ipfs.io/ipns/12D3KooWCcbggDkc8B25u51XktYSBv1b8g9W31Zii5ovyJPHJ86X).
* [/ipns/mfs.dapp.rabrux.space](https://gateway.ipfs.io/ipns/mfs.dapp.rabrux.space).

comming soon.

## description

this is my personal journey and i guess that it converts or creates in something more usable that only a [reactjs](https://reactjs.org) and [ipfs](https://ipfs.io) exercise.

then this micro dapp only executes the little task of manage your files stored in the [mutabe file system](https://github.com/ipfs/interface-js-ipfs-core/blob/master/SPEC/FILES.md#the-files-api-aka-mfs-the-mutable-file-system) of ipfs.

## technologies

* [reactjs](https://reactjs.org)
* [ipfs](https://ipfs.io)

## depends on

* [@ipui/ipfs](https://gitlab.com/ipui/ipui-ipfs)
* [@ipui/path](https://gitlab.com/ipui/ipui-path)
* [@ipui/clipboard](https://gitlab.com/ipui/ipui-clipboard)
* [@ipui/skin](https://gitlab.com/ipui/ipui-skin)

## how it works

* it uses the [context api](https://reactjs.org/docs/context.html) of reactjs to provide an accessible service to any component inside the context itself.

* uses `@ipui/path` as a router to navigate between folders.

* uses `@ipui/ipfs` to provide an instance of [ipfs-http-client](https://github.com/ipfs/js-ipfs-http-client).

* uses `@ipui/clipboard` to perform `copy`/`paste` actions in whole directory tree.

* binds `withDir` on the `props` or `this.props` when is used as _consumer_.

## dev

> This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## contribute

1. create a issue.
2. report a issue.
3. review the code.
4. resolve an issue.

## last update
aug 25, 2019
