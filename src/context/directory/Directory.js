import React, { Component, createContext } from 'react'
import PropTypes from 'prop-types'

import { withIpfs } from '@ipui/ipfs'
import { withPath } from '@ipui/path'

const DirContext = createContext()

class Directory extends Component {

  static propTypes = {
    children: PropTypes.node.isRequired
  }

  state = {
    current: null,
    files: null
  }

  constructor( props ) {
    super( props )
    this.cd = this.cd.bind( this )
    this.refresh = this.refresh.bind( this )
    this.ls = this.ls.bind( this )
  }

  componentDidMount() {
    const { current } = this.props.withPath
    this.ls( current )
  }

  ls( path ) {
    const { stat, ls } = this.props.withIpfs.node.files
    const { go } = this.props.withPath

    stat( path )
      .then( stats => {
        this.setState( {
          ...this.state,
          current: {
            ...stats,
            fullpath: path
          }
        } )

        go( path )

        return ls( path )
      } )
      .then( files => {
        this.setState( {
          ...this.state,
          files: files
        } )
      } )

  }

  shouldComponentUpdate( nextProps, nextState ) {
    const { files, current } = nextState
    if ( !files || !current )
      return false
    return true
  }

  cd( dir ) {
    const { dirname, join, current } = this.props.withPath
    if ( dir === '..' )
      dir = dirname( current )
    else
      dir = join( current, dir )

    this.ls( dir )
  }

  refresh() {
    const { fullpath } = this.state.current
    this.ls( fullpath )
  }

  render() {
    const { current, files } = this.state

    if ( !current || !files )
      return ( <>loading...</> )

    const { children } = this.props
    const { cd, refresh } = this
    const {
      cp,
      mkdir,
      mv,
      read,
      rm,
      stat,
      write
    } = this.props.withIpfs.node.files
    const {
      join,
      dirname,
      filename
    } = this.props.withPath

    return (
      <DirContext.Provider value={ {
        current,
        files,
        cd,
        refresh,

        join,
        filename,
        dirname,

        cp,
        mkdir,
        mv,
        read,
        rm,
        stat,
        write
      } }>
        { children }
      </DirContext.Provider>
    )
  }
}

const withDir = ( ComponentAlias ) => {

  return props => (
    <DirContext.Consumer>
      { context => {
        return <ComponentAlias { ...props } withDir={ context } />
      } }
    </DirContext.Consumer>
  )

}

export default withPath( withIpfs( Directory ) )

export {
  withDir
}
