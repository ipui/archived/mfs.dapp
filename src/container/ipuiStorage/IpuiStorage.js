import React from 'react'

import Ipfs from '@ipui/ipfs'
import Clipboard from '@ipui/clipboard'

import Path from '@ipui/path'
import Directory from '../../context/directory/Directory'

import Settings from '../../component/settings/Settings'
import DirList from '../../component/directory/dirList/DirList'
import Toolbar from '../../component/toolbar/Toolbar'

import NoIpfs from '../../component/noIpfs/NoIpfs'

const IpuiStorage = props => {
  return (
    <Path>
      <Ipfs noIpfs={ NoIpfs }>
        <Clipboard config='/.config/storage/clipboard'>
          <Directory>

            <Settings />

            <DirList />

            <Toolbar />

          </Directory>
        </Clipboard>
      </Ipfs>
    </Path>
  )
}

export default IpuiStorage

