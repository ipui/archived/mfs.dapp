import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './DevilsFive.sass'

class DevilsFive extends Component {

  state = {
    timeout: undefined,
    count: 5
  }

  static propTypes = {
    toggle: PropTypes.func.isRequired,
    action: PropTypes.func.isRequired,
    label: PropTypes.string
  }

  componentDidMount() {
    this.registerTick( this.tick )
  }

  registerTick( tick ) {
    this.setState( {
      ...this.state,
      count: this.state.count - 1,
      timeout: setTimeout( () => {
        this.tick()
      }, 1000 )
    } )
  }

  tick() {
    const { action, toggle } = this.props
    const { count } = this.state
    if ( count > 0 )
      return this.registerTick()

    action()
    toggle()
  }

  take() {
    const { timeout } = this.state
    const { toggle } = this.props
    clearTimeout( timeout )
    toggle()
  }

  render() {
    const { count } = this.state
    const label = this.props.label || ''
    
    return (
      <button className="devils-five" onClick={ this.take.bind( this ) }>
        { label } <span>{ count }</span>
      </button>
    )
  }

}

export default DevilsFive
