import React, { useState } from 'react'

import {
  FiSettings,
  FiX
} from 'react-icons/fi'

import IpfsConnect from '../ipfs-connect/IpfsConnect'
import Name from './name/Name'

const Settings = props => {

  const [ isToggled, setIsToggled ] = useState( false )

  function toggleSettings() {
    setIsToggled( !isToggled )
  }

  return (
    <>

      <header>

        <Name />

        <button onClick={ toggleSettings }>
          { isToggled ? ( <FiX /> ) : ( <FiSettings /> ) }
        </button>

      </header>

      { isToggled ? (
        <header>
          <IpfsConnect />
        </header>
      ) : null }

    </>
  )

}

export default Settings
