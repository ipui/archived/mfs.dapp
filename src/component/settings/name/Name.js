import React, { Component } from 'react'

import { withIpfs } from '@ipui/ipfs'
import { withPath } from '@ipui/path'

import DirGoUp from '../../directory/dirGoUp/DirGoUp'

class Name extends Component {

  state = {
    stats: {
      name: '/'
    },
    isToggled: false
  }

  constructor( props ) {
    super( props )
    this.stats = this.stats.bind( this )
    this.toggle = this.toggle.bind( this )
    this.updateName = this.updateName.bind( this )
  }

  componentDidMount() {
    const { current } = this.props.withPath
    this.updateName( current )
  }

  componentWillUpdate( nextProps, nextState ) {
    const { current } = this.props.withPath
    const next = nextProps.withPath.current
    if( current === next )
      return

    this.updateName( next )
  }

  updateName( path ) {
    const { filename } = this.props.withPath

    this
      .stats( path )
      .then( stats => {
        const name = filename( path ) || '/'
        this.setState( {
          ...this.state,
          stats: {
            ...stats,
            fullpath: path,
            name: name
          }
        } )
      } )
      .catch( err => {
        console.error( 'path -> stat', err )
      } )
  }

  stats( path ) {
    const { node } = this.props.withIpfs
    return node.files.stat( path, { hash: true } )
  }

  toggle() {
    this.setState( {
      ...this.state,
      isToggled: !this.state.isToggled
    } )
  }

  render() {
    const { hash, name } = this.state.stats
    if ( !hash )
      return ( <i>...</i> )

    const { isToggled } = this.state
    const { toggle } = this

    return (
    <>

      <DirGoUp />

      { isToggled ? (
        <h1 onClick={ toggle }>{ hash }</h1>
      ) : (
        <h1 onClick={ toggle }>{ name }</h1>
      ) }
    </>
    )
  }

}

export default withIpfs( withPath( Name ) )
