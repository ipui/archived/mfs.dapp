import React from 'react'

import { withDir } from '../../../context/directory/Directory'

import { FiCornerLeftUp } from 'react-icons/fi'

const DirGoUp = props => {

  function handleClick() {
    const { cd } = props.withDir
    cd( '..' )
  }

  const { fullpath } = props.withDir.current

  return (
    <>
      { fullpath !== '/' ? (
        <button onClick={ handleClick }>
          <FiCornerLeftUp />
        </button>
      ) : null }
    </>
  )
}

export default withDir( DirGoUp )
