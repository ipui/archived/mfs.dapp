import React from 'react'

import { FiInfo } from 'react-icons/fi'

import './DirEmpty.sass'

const DirEmpty = props => {

  return (
    <main className="dir-empty">
      <figure>
        <FiInfo />
      </figure>
      <p>this folder is empty, you can use the bottom toolbar to create or add files and folders.</p>
    </main>
  )
}

export default DirEmpty

