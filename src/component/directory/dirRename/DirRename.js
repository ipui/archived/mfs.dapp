import React, { useState } from 'react'
import PropTypes from 'prop-types'

import { withDir } from '../../../context/directory/Directory'

import {
  FiCheck,
  FiX
} from 'react-icons/fi'

const DirRename = props => {

  const [ newName, setNewName ] = useState( '' )
  const { toggle } = props

  function updateName( event ) {
    const { value } = event.target
    setNewName( value )
  }

  function reset() {
    setNewName( '' )
    toggle()
  }

  function rename() {
    const { fullpath } = props
    const { mv, dirname, join } = props.withDir
    const parent = dirname( fullpath )
    const newPath = join( parent, newName )
    mv( fullpath, newPath, {
      parents: true
    } )
      .then( () => {
        const { refresh } = props.withDir
        reset()
        refresh()
      } )
  }

  const { placeholder } = props

  return (
    <>
      <input
        placeholder={ placeholder || 'rename' }
        onChange={ updateName }
        onKeyDown={ e => {
          if ( e.keyCode === 13 )
            rename()
        } }
        size="1"
        autoFocus
      />
      <button onClick={ rename }>
        <FiCheck />
      </button>
      <button onClick={ reset }>
        <FiX />
      </button>
    </>
  )
}

DirRename.propTypes = {
  fullpath: PropTypes.string.isRequired,
  toggle: PropTypes.func.isRequired,
  placeholder: PropTypes.string
}

export default withDir( DirRename )
