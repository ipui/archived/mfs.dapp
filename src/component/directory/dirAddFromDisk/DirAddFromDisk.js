import React from 'react'
import PropTypes from 'prop-types'

import { withDir } from '../../../context/directory/Directory'

import './DirAddFromDisk.sass'

const DirAddFromDisk = props => {

  function add( event ) {
    const { files } = event.target
    loadFiles( files )
  }

  function loadFiles( files ) {
    if( files instanceof FileList === false )
      return

    let filesPromises = []
    for( let i = 0; i < files.length; i++ ) {
      const file = files[ i ]
      filesPromises.push( readFile( file ) )
    }
    Promise.all( filesPromises )
      .then( () => {
        const { refresh } = props.withDir
        const { toggle } = props
        refresh()
        toggle()
      } )
      .catch( err => {
        console.error( 'loadFiles all', err )
      } )
  }

  function readFile( file ) {

    return new Promise( ( resolve, reject ) => {
      const reader = new FileReader()
      reader.onload = ( event ) => {
        const content = event.target.result
        resolve( writeFile( file, content ) )
      }
      reader.onerror = reject
      reader.readAsArrayBuffer( file )
    } )
  }

  function writeFile( file, content ) {
    const { write } = props.withDir
    const path = makePath( file )
    return write( path, new Buffer( content ), {
      create: true,
      parents: true
    } )
  }

  function makePath( file ) {
    const { current, join } = props.withDir
    const { dir } = props
    const { webkitRelativePath, name } = file

    if ( dir )
      return join( current.fullpath, webkitRelativePath )

    return join( current.fullpath, name )
  }

  const { children, dir } = props

  return (
    <label className="dir-add-from-disk">
      { dir ? (
        <input
          type="file"
          onChange={ add }
          mozdirectory="true"
          webkitdirectory="true"
          directory="true"
        />
      ) : (
        <input
          type="file"
          multiple
          onChange={ add }
        />
      ) }
      { children }
    </label>
  )
}

DirAddFromDisk.propTypes = {
  toggle: PropTypes.func.isRequired,
  dir: PropTypes.bool
}

export default withDir( DirAddFromDisk )
