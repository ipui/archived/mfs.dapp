import React from 'react'

import { withPath } from '@ipui/path'

import { FaICursor } from 'react-icons/fa'

const DirAddFromEditor = props => {

  function handleOpenEditor() {
    const { api, current } = props.withPath
    const redirectPath = 'http://127.0.0.1:8080/ipns/12D3KooWNTU5TKRgJezC1y9Q8iXXMpRLFynhKZFXtAubwZNGo5KB/#' + api + current
    window.location.replace( redirectPath )
  }

  return (
    <button onClick={ handleOpenEditor }>
      <figure>
        <FaICursor />
      </figure>
      <label>editor</label>
    </button>
  )

}

export default withPath( DirAddFromEditor )
