import React, { Component } from 'react'
import PropTypes from 'prop-types'
import filetype from 'file-type'

import { withIpfs } from '@ipui/ipfs'
import { withPath } from '@ipui/path'

import { FaICursor } from 'react-icons/fa'

class DirOpenWithEditor extends Component {

  state = {
    isTextFile: false
  }

  constructor( props ) {
    super( props )
    this.handleOpen = this.handleOpen.bind( this )
  }

  componentDidMount() {
    const { fullpath, type } = this.props.file

    if ( type !== 'file' )
      return

    const { read } = this.props.withIpfs.node.files
    read( fullpath, {
      offset: 0,
      length: 32
    } )
      .then( buf => {
        const type = filetype( buf )
        if ( type )
          return

        this.setState( {
          ...this.state,
          isTextFile: true
        } )
      } )
      .catch( err => {
        console.error( 'error', err )
      } )
  }

  handleOpen() {
    const { api } = this.props.withPath
    const { fullpath } = this.props.file
    const { toggle } = this.props
    const redirectPath = 'http://127.0.0.1:8080/ipns/12D3KooWNTU5TKRgJezC1y9Q8iXXMpRLFynhKZFXtAubwZNGo5KB/#' + api + fullpath
    window.location.replace( redirectPath )
    toggle()
  }

  render() {
    const { isTextFile } = this.state
    const { type } = this.props.file
    const { handleOpen } = this

    return (
      <>
        { isTextFile && type === 'file' ? (
          <button onClick={ handleOpen }>
            <FaICursor />
          </button>
        ) : null }
      </>
    )
  }
}

DirOpenWithEditor.propTypes = {
  toggle: PropTypes.func.isRequired,
  file: PropTypes.object.isRequired
}

export default withPath( withIpfs( DirOpenWithEditor ) )
