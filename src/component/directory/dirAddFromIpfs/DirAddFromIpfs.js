import React, { useState } from 'react'
import PropTypes from 'prop-types'
import CID from 'cids'

import { withDir } from '../../../context/directory/Directory'

import {
  FiArrowRight,
  FiX,
  FiHelpCircle
} from 'react-icons/fi'

// import './DirAddFromIpfs.sass'

const FromIpfs = props => {

  const re = /^(Qm[a-zA-Z0-9]{44}|[a-zA-Z0-9]+|\/ipfs\/(Qm[a-zA-Z0-9]{44}|[a-zA-Z0-9]+))/

  const [ address, setAddress ] = useState( '' )
  const [ isHelpOpen, setIsHelpOpen ] = useState( false )
  const { toggle } = props

  function toggleHelp() {
    setIsHelpOpen( !isHelpOpen )
  }

  function reset() {
    setAddress( '' )
    toggle()
  }

  function add() {
    const isValidAddress = validateAddress( address )
    if ( !isValidAddress )
      return console.error( 'invalid address' )

    const { cp } = props.withDir
    const { current, join } = props.withDir

    const cidAddr = getCid( address )

    try {
      const cid = new CID( cidAddr )
      const path = join( current.fullpath, cid.toBaseEncodedString() )

      cp( address, path )
        .then( () => {
          const { refresh } = props.withDir
          const { onAdd } = props
          refresh()
          onAdd( path )
        } )
        .catch( err => {
          console.error( 'add cp', err )
        } )
    } catch ( e ) {
      console.error( 'updateAddress cid', e )
    }
  }

  function validateAddress( value ) {
    return re.test( value )
  }

  function getCid( value ) {
    const parts = value.match( re )

    let cid = parts[ 1 ]
    if ( parts[ 2 ] )
      cid = parts[ 2 ]

    return cid
  }

  function updateAddress( event ) {
    const { value } = event.target
    setAddress( value )
  }

  return (
    <section className="dir-add-from-ipfs">

      <header>
        <button onClick={ toggleHelp }>
          <FiHelpCircle />
        </button>

        <input
          placeholder="Qm..."
          size="1"
          autoFocus
          onChange={ updateAddress }
          onKeyDown={ e => {
            if ( e.keyCode === 13 )
              add()
          } }
        />

        <button onClick={ add }>
          <FiArrowRight />
        </button>

        <button onClick={ reset }>
          <FiX />
        </button>
      </header>

      { isHelpOpen ? (
        <section className="add-from-ipfs-help">
          <cite>
            A content identifier, or CID, is a label used
            to point to material in IPFS. It doesn’t indicate
            where the content is stored, but it forms a kind
            of address based on the content itself.

          </cite>
          <a href="http://127.0.0.1:8080/ipns/docs.ipfs.io/guides/concepts/cid/" target="_blank">Content Identifiers</a>
          <p>
            <i>examples</i>
          </p>
          <code>/ipfs/QmZTR5bcpQD7cFgTorqxZDYaew1Wqgfbd2ud9QqGPAkK2V</code>
          <code>QmPZ9gcCEpqKTo6aq61g2nXGUhM4iCL3ewB6LDXZCtioEB</code>
        </section>
      ) : null }

    </section>

  )
}

FromIpfs.propTypes = {
  toggle: PropTypes.func.isRequired,
  onAdd: PropTypes.func
}

export default withDir( FromIpfs )
