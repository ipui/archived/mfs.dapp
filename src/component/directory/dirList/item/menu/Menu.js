import React from 'react'
import PropTypes from 'prop-types'

import ClipPut from '../../../../clip/clipPut/ClipPut'
import DirOpenWithEditor from '../../../dirOpenWithEditor/DirOpenWithEditor'

import {
  FiEdit2,
  FiHexagon,
  FiCopy,
  FiShare2,
  FiTrash,
  FiScissors
} from 'react-icons/fi'

const Menu = props => {

  const {
    file,
    toggleRename,
    toggleShare,
    toggleRemove,
    toggle
  } = props

  return (
    <menu>

      <DirOpenWithEditor file={ file } toggle={ toggle } />

      <button onClick={ toggleShare }>
        <FiShare2 />
      </button>

      <button onClick={ toggleRename }>
        <FiEdit2 />
      </button>

      <ClipPut file={ file } toggle={ toggle }>
        <FiCopy />
      </ClipPut>

      <ClipPut file={ file } toggle={ toggle } move>
        <FiScissors />
      </ClipPut>

      <button onClick={ e => console.log( file.hash ) }>
        <FiHexagon />
      </button>

      <button onClick={ toggleRemove }>
        <FiTrash />
      </button>

    </menu>
  )
}

Menu.propTypes = {
  file: PropTypes.object.isRequired,
  toggleRename: PropTypes.func.isRequired,
  toggleShare: PropTypes.func.isRequired,
  toggleRemove: PropTypes.func.isRequired,
  toggle: PropTypes.func.isRequired
}

export default Menu
