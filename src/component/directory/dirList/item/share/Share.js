import React from 'react'
import PropTypes from 'prop-types'

import {
  FiUser,
  FiUsers,
  FiX
} from 'react-icons/fi'

import { IoMdGlobe } from 'react-icons/io'

const Share = props => {

  const { toggle } = props

  return (
    <>
      <button>
        <FiUser />
      </button>
      <button>
        <FiUsers />
      </button>
      <button>
        <IoMdGlobe />
      </button>
      <button onClick={ toggle }>
        <FiX />
      </button>
    </>
  )
}

Share.propTypes = {
  toggle: PropTypes.func.isRequired
}

export default Share
