import React, { Component } from 'react'
import PropTypes from 'prop-types'
import CID from 'cids'

import {
  FiMoreHorizontal,
  FiFolder,
  FiFile,
  FiHexagon,
  FiX
} from 'react-icons/fi'

import Menu from './menu/Menu'
import DirRename from '../../dirRename/DirRename'
import Share from './share/Share'
import DevilsFive from '../../../devilsFive/DevilsFive'

import './Item.sass'

class Item extends Component {

  static propTypes = {
    file: PropTypes.object.isRequired,
    withDir: PropTypes.object.isRequired
  }

  constructor( props ) {
    super( props )
    this.state = {
      file: props.file,
      stats: {},
      isMenuOpen: false,
      isRenameOpen: false,
      isShareOpen: false,
      isRemoveOpen: false
    }
    this.toggleMenu = this.toggleMenu.bind( this )
    this.toggleRename = this.toggleRename.bind( this )
    this.toggleShare = this.toggleShare.bind( this )
    this.toggleRemove = this.toggleRemove.bind( this )
    this.open = this.open.bind( this )
    this.cd = this.cd.bind( this )
    this.remove = this.remove.bind( this )

    this.isCid = this.isCid.bind( this )
    this.openInGateway = this.openInGateway.bind( this )
  }

  componentDidMount() {
    const { name, fullpath } = this.state.file
    const { stat } = this.props.withDir
    const isCid = this.isCid( name )
    if ( isCid && isCid.codec === 'dag-cbor' ) {
      this.setState( {
        ...this.state,
        stats: {
          type: isCid.codec
        }
      } )
    } else {
      stat( fullpath )
        .then( stats => {
          this.setState( {
            ...this.state,
            stats
          } )
        } )
    }
  }

  isCid( cidString ) {
    try {
      const cid = new CID( cidString )
      return cid
    } catch ( e ) {
      return false
    }
  }

  toggleMenu() {
    const { isMenuOpen } = this.state

    if ( !isMenuOpen ) {
      this.setState( {
        ...this.state,
        isMenuOpen: true,
        isRenameOpen: false,
        isShareOpen: false,
        isRemoveOpen: false
      } )
    } else {
      this.setState( {
        ...this.state,
        isMenuOpen: false,
        isRenameOpen: false,
        isShareOpen: false,
        isRemoveOpen: false
      } )
    }

  }

  toggleRename() {
    this.setState( {
      ...this.state,
      isRenameOpen: !this.state.isRenameOpen
    } )
  }

  toggleShare() {
    this.setState( {
      ...this.state,
      isShareOpen: !this.state.isShareOpen
    } )
  }

  toggleRemove() {
    this.setState( {
      ...this.state,
      isRemoveOpen: !this.state.isRemoveOpen
    } )
  }

  remove() {
    const { fullpath } = this.props.file
    const { rm, refresh } = this.props.withDir
    rm( fullpath, {
      recursive : true
    } )
      .then( () => {
        refresh()
      } )

  }

  openInGateway( hash ) {
    const address = 'http://127.0.0.1:8080/ipfs/' + hash
    window.open( address )
  }

  open() {
    const { type, hash } = this.state.stats
    const { name } = this.props.file
    const { cd, openInGateway } = this
    switch( type ) {
      case 'directory':
        if( /\.dapp$/.test( name ) )
          openInGateway( hash )
        else
          cd()
        break
      case 'file':
        openInGateway( hash )
        break
      case 'dag-cbor':
        console.log( 'open cbor' )
        break
      default:
        console.error( 'unhandled filetype', type )
    }
  }

  cd() {
    const { name } = this.props.file
    const { cd } = this.props.withDir
    cd( name )
  }

  render() {
    const {
      file,
      stats,
      isMenuOpen,
      isRenameOpen,
      isShareOpen,
      isRemoveOpen
    } = this.state
    const { name, fullpath } = file
    const { type, hash } = stats

    if ( typeof name === 'undefined' )
      return ( <li>loading</li> )

    const {
      toggleMenu,
      toggleRename,
      toggleShare,
      toggleRemove,
      remove,
      open
    } = this

    return (
      <>
        <li className="file-list-item">
          <figure>
            { type === 'directory' ? (
              <FiFolder />
            ) : null }

            { type === 'file' ? (
              <FiFile />
            ) : null }

            { type === 'dag-cbor' ? (
              <FiHexagon />
            ) : null }
          </figure>

          { isRenameOpen ? (
            <>
              <DirRename
                toggle={ toggleMenu }
                placeholder={ name }
                fullpath={ fullpath }
              />
            </>
          ) : null }

          { !isRenameOpen ? (
            <>
              <label onClick={ open }>
                { name }
              </label>

              { isMenuOpen && !isRenameOpen && !isShareOpen && !isRemoveOpen ? (
                <>
                  <Menu
                    file={ {
                      ...file,
                      ...stats
                    } }
                    toggleRename={ toggleRename }
                    toggleShare={ toggleShare }
                    toggleRemove={ toggleRemove }
                    toggle={ toggleMenu }
                  />

                </>
              ) : null }

              { isShareOpen ? (
                <>
                  <Share
                    toggle={ toggleMenu }
                    cid={ hash }
                  />
                </>
              ) : null }

              { isRemoveOpen ? (
                <>
                  <DevilsFive
                    toggle={ toggleMenu }
                    action={ remove.bind( this ) }
                    label="recovery"
                  />
                </>
              ) : null }

              { !isShareOpen && !isRemoveOpen ? (
                <button onClick={ toggleMenu }>
                  { isMenuOpen ? (
                    <FiX />
                  ) : (
                    <FiMoreHorizontal />
                  ) }
                </button>
              ) : null }

            </>
          ) : null }

        </li>

      </>
    )
  }

}

export default Item
