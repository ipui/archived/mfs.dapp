import React from 'react'

import { withDir } from '../../../context/directory/Directory'

import Item from './item/Item'
import DirEmpty from '../dirEmpty/DirEmpty'

const DirList = props => {

  const { files, current, join } = props.withDir
  const items = files.map( file => {
    const { hash, name } = file
    return (
      <Item
        key={ hash + name }
        file={ {
          ...file,
          fullpath: join( current.fullpath, name )
        } }
        withDir={ props.withDir }
      />
    )
  } )

  if ( items.length === 0 )
    return ( <DirEmpty /> )

  return (
    <main>
      <ul>
        { items }
      </ul>
    </main>
  )
}

export default withDir( DirList )
