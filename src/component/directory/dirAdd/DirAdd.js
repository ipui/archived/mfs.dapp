import React, { useState } from 'react'
import PropTypes from 'prop-types'

import {
  FiFile,
  FiFolder,
  FiHexagon,
  FiX
} from 'react-icons/fi'

import DirAddFromEditor from '../dirAddFromEditor/DirAddFromEditor'
import DirAddFromDisk from '../dirAddFromDisk/DirAddFromDisk'
import DirAddFromIpfs from '../dirAddFromIpfs/DirAddFromIpfs'
import DirRename from '../dirRename/DirRename'

import './DirAdd.sass'

const DirAdd = props => {

  const [ isAddFromIpfsOpen, setIsAddFromIpfsOpen ] = useState( false )
  const [ isRenameOpen, setIsRenameOpen ] = useState( false )
  const [ fromIpfs, setFromIpfs ] = useState( null )

  function toggleAddFromIpfs() {
    setIsAddFromIpfsOpen( !isAddFromIpfsOpen )
  }

  function toggleRename( path ) {
    setFromIpfs( path )
    setIsRenameOpen( !isRenameOpen )
  }

  const { toggle } = props

  return (
    <section className="dir-add">
      { isAddFromIpfsOpen ? (
        <>
          { isRenameOpen ? (
            <section className="dir-add-from-ipfs">
              <header>
                <DirRename fullpath={ fromIpfs } toggle={ toggle } />
              </header>
            </section>
          ) : (
            <DirAddFromIpfs toggle={ toggle } onAdd={ toggleRename } />
          ) }
        </>
      ) : (
        <>
          <header>
            <button onClick={ toggle }>
              <FiX />
            </button>
          </header>

          <ul>

            <li>
              <DirAddFromEditor toggle={ toggle } />
            </li>

            <li>
              <DirAddFromDisk toggle={ toggle }>
                <figure>
                  <FiFile />
                </figure>
                file
              </DirAddFromDisk>
            </li>

            <li>
              <DirAddFromDisk dir toggle={ toggle }>
                <figure>
                  <FiFolder />
                </figure>
                folder
              </DirAddFromDisk>
            </li>

            <li>
              <button onClick={ toggleAddFromIpfs }>
                <figure>
                  <FiHexagon />
                </figure>
                <label>from ipfs</label>
              </button>
            </li>

          </ul>
        </>
      ) }
    </section>
  )
}

DirAdd.propTypes = {
  toggle: PropTypes.func.isRequired
}

export default DirAdd
