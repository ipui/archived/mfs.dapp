import React, { useState } from 'react'
import PropTypes from 'prop-types'

import { withDir } from '../../../context/directory/Directory'

import {
  FiCheck,
  FiX
} from 'react-icons/fi'

const DirCreate = props => {

  const [ newName, setNewName ] = useState( '' )

  function update( event ) {
    setNewName( event.target.value )
  }

  function create() {
    if ( newName.length === 0 )
      return

    const { mkdir, join, current } = props.withDir
    const newPath = join( current.fullpath, newName )

    mkdir( newPath, {
      parents: true
    } )
      .then( () => {
        const { refresh } = props.withDir
        refresh()
        reset()
      } )
      .catch( err => {
        console.error( 'create directory', err )
      } )
  }

  function reset() {
    const { toggle } = props
    setNewName( '' )
    toggle()
  }

  return (
    <footer>

      <input
        value={ newName }
        placeholder="folder name"
        onChange={ update }
        onKeyDown={ event => {
          if ( event.keyCode === 13 )
            create()
        } }
        size="1"
        autoFocus
      />
      <button onClick={ create }>
        <FiCheck />
      </button>
      <button onClick={ reset }>
        <FiX />
      </button>

    </footer>
  )
}

DirCreate.propTypes = {
  toggle: PropTypes.func.isRequired
}

export default withDir( DirCreate )
