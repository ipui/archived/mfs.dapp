import React from 'react'
import PropTypes from 'prop-types'

import { withClipboard } from '@ipui/clipboard'

const ClipClean = props => {

  function _clean() {
    const { clean } = props.withClipboard
    const { onClean } = props
    clean()
    if ( typeof onClean === 'function' )
      onClean()
  }

  const { children } = props

  return (
    <button onClick={ _clean }>
      { children }
    </button>
  )
}

ClipClean.propTypes = {
  children: PropTypes.node.isRequired,
  onClean: PropTypes.func
}

export default withClipboard( ClipClean )

