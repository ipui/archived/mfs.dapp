import React from 'react'
import PropTypes from 'prop-types'

import { withClipboard } from '@ipui/clipboard'

const Remove = props => {

  function fileIndex( file ) {
    const { clip } = props.withClipboard
    const item = clip.filter( el => el.fullpath === file.fullpath ).shift()
    if ( !item )
      return -1

    return clip.indexOf( item )
  }

  function handleClick() {
    const { remove } = props.withClipboard
    const { file } = props

    const index = fileIndex( file )
    if ( index >= 0 )
      remove( index )
  }

  const { children } = props

  return (
    <button onClick={ handleClick }>
      { children }
    </button>
  )
}

Remove.propTypes = {
  children: PropTypes.node.isRequired,
  file: PropTypes.object.isRequired
}

export default withClipboard( Remove )
