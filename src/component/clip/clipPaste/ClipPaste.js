import React from 'react'
import PropTypes from 'prop-types'

import { withDir } from '../../../context/directory/Directory'
import { withClipboard } from '@ipui/clipboard'

const ClipPaste = props => {

  const { children } = props

  function fileIndex( file ) {
    const { clip } = props.withClipboard
    const item = clip.filter( el => el.fullpath === file.fullpath ).shift()
    if ( !item )
      return -1

    return clip.indexOf( item )
  }

  function paste( file, index ) {
    const { fullpath, move } = file
    const { mv, cp, join, current, filename, refresh } = props.withDir
    const { remove } = props.withClipboard

    let action = cp
    if ( move )
      action = mv

    const name = filename( fullpath )
    const newPath = join( current.fullpath, name )
    action( fullpath, newPath, {
      parents: true
    } )
      .then( () => {
        refresh()
        remove( index )
      } )

  }

  function handleClick() {
    const { file } = props

    const index = fileIndex( file )
    if ( index >= 0 )
      paste( file, index )
  }

  return (
    <button onClick={ handleClick }>
      { children }
    </button>
  )
}

ClipPaste.propTypes = {
  children: PropTypes.node.isRequired,
  file: PropTypes.object.isRequired
}

export default withDir( withClipboard( ClipPaste ) )
