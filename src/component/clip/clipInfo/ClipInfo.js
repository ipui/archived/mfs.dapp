import React from 'react'
import PropTypes from 'prop-types'

import { withClipboard } from '@ipui/clipboard'

import './ClipInfo.sass'

const ClipInfo = props => {
  const { children, toggle } = props
  const { clip } = props.withClipboard

  return (
    <button className="clip-info" onClick={ toggle }>
      { children }
      <sup>
        { clip.length || null }
      </sup>
    </button>
  )
}

ClipInfo.propTypes = {
  children: PropTypes.node.isRequired,
  toggle: PropTypes.func.isRequired
}

export default withClipboard( ClipInfo )
