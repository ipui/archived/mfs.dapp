import React from 'react'
import PropTypes from 'prop-types'

import ClipRemove from '../clipRemove/ClipRemove'
import ClipPaste from '../clipPaste/ClipPaste'

import { FiXCircle } from 'react-icons/fi'

import {
  FiFile,
  FiFolder,
  FiHexagon
} from 'react-icons/fi'

import {
  FaPaste,
} from 'react-icons/fa'

const ClipFile = props => {

  const {
    name,
    hash,
    type
  } = props.file

  return (
    <li key={ name + hash }>

      <figure>
        { type === 'directory' ? (
          <FiFolder />
        ) : null }

        { type === 'file' ? (
          <FiFile />
        ) : null }

        { type === 'dag-cbor' ? (
          <FiHexagon />
        ) : null }
      </figure>

      <label>{ name }</label>

      <ClipRemove file={ props.file }>
        <FiXCircle />
      </ClipRemove>

      <ClipPaste file={ props.file }>
        <FaPaste />
      </ClipPaste>

    </li>
  )
}

ClipFile.propTypes = {
  file: PropTypes.object.isRequired
}

export default ClipFile

