import React from 'react'
import PropTypes from 'prop-types'

import { withClipboard } from '@ipui/clipboard'

const ClipPut = props => {

  function inClip( file ) {
    const { clip } = props.withClipboard

    const item = clip.filter( el => {
      const re = new RegExp( '^' + el.fullpath )
      return re.test( file.fullpath )
    } ).shift()

    return item || false
  }

  function removeChilds( file ) {
    const { clip, remove } = props.withClipboard

    const re = new RegExp( '^' + file.fullpath )
    const childs = clip.filter( el => {
      return re.test( el.fullpath )
    } )

    childs.forEach( el => {
      const index = clip.indexOf( el )
      remove( index )
    } )

  }

  function putFile() {
    const { put } = props.withClipboard
    const { file, move, toggle } = props

    removeChilds( file )

    put( {
      ...file,
      move: move || false
    } )
    toggle()
  }

  const { children, file } = props

  const isInClip = inClip( file )
  if ( isInClip )
    return ( <></> )

  return(
    <button onClick={ putFile }>
      { children }
    </button>
  )

}

ClipPut.propTypes = {
  file: PropTypes.any.isRequired,
  toggle: PropTypes.func.isRequired,
  move: PropTypes.bool
}

export default withClipboard( ClipPut )
