import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { withClipboard } from '@ipui/clipboard'
import ClipFile from './clipFile/ClipFile'
import ClipClean from './clipClean/ClipClean'

import {
  FiMinus,
  FiTrash,
  FiMaximize2,
  FiMinimize2
} from 'react-icons/fi'

import './Clip.sass'

class Clip extends Component {

  state = {
    isFullScreen: false
  }

  constructor( props ) {
    super( props )
    this.toggleFullScreen = this.toggleFullScreen.bind( this )
  }

  toggleFullScreen() {
    this.setState( {
      ...this.state,
      isFullScreen: !this.state.isFullScreen
    } )
  }

  render() {
    // console.log( 'render clip-files', this.props )
    const { toggle, isOpen } = this.props
    const { clip } = this.props.withClipboard
    const { toggleFullScreen } = this
    const { isFullScreen } = this.state

    if ( clip.length === 0 ) {
      if ( isOpen )
        toggle()
      return ( <></> )
    }

    const files = clip.map( file => {
      return (
        <ClipFile
          key={ file.hash + file.name }
          file={ file }
        />
      )
    } )

    let classes = [ 'clip'  ]

    if ( isFullScreen )
      classes.push( 'fullscreen' )

    return (

      <section className={ classes.join( ' ' ) }>

        <header>
          <button onClick={ toggle }>
            <FiMinus />
          </button>

          <button onClick={ toggleFullScreen }>
            { isFullScreen ? (
              <FiMinimize2 />
            ) : (
              <FiMaximize2 />
            ) }
          </button>

          <ClipClean onClean={ toggle }>
            <FiTrash />
          </ClipClean>
        </header>

        <section>
          <ul>
            { files }
          </ul>
        </section>

      </section>
    )
  }
}

Clip.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired
}

export default withClipboard( Clip )

