import React from 'react'

import {
  FiHeart,
  FiGitlab
} from 'react-icons/fi'

import {
  IoIosBug
} from 'react-icons/io'

import './NoIpfs.sass'

const NoIpfs = props => {

  return (
    <section className="no-ipfs">
      <p>
      an <a href="https://ipfs.io" target="_blank">ipfs</a> node is required to use this dapp.
      </p>
      <p>
        made with <FiHeart />
      </p>
      <ul>
        <li>
          <a href="https://gitlab.com/ipui/mfs.dapp" target="_blank">
            <FiGitlab /> source code
          </a>
        </li>
        <li>
          <a href="https://gitlab.com/ipui/mfs.dapp/issues" target="_blank">
            <IoIosBug /> contribute
          </a>
        </li>
      </ul>
    </section>
  )
}

export default NoIpfs
