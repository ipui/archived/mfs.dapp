import React from 'react'

import {
  FiMoon,
  FiSun
} from 'react-icons/fi'

import { withSkin } from '@ipui/skin'

const ToggleTheme = props => {

  const { theme, toggle } = props

  return (
    <button onClick={ toggle }>
      { theme === 'moon' ? ( <FiSun /> ) : ( <FiMoon /> ) }
    </button>
  )
}

export default withSkin( ToggleTheme )

