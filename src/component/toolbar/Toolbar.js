import React, { useState } from 'react'

import Clip from '../clip/Clip'
import ClipInfo from '../clip/clipInfo/ClipInfo'

import DirCreate from '../directory/dirCreate/DirCreate'
import DirAdd from '../directory/dirAdd/DirAdd'

import {
  FiPlus,
  FiFolder,
  FiClipboard
} from 'react-icons/fi'

const ToolBar = props => {

  const [ isClipOpen, setIsClipOpen ] = useState( false )
  const [ isDirCreateOpen, setIsDirCreateOpen ] = useState( false )
  const [ isAddOpen, setIsAddOpen ] = useState( false )

  function toggleClip() {
    setIsClipOpen( !isClipOpen )
  }

  function toggleDirCreate() {
    setIsDirCreateOpen( !isDirCreateOpen )
  }

  function toggleAdd() {
    setIsAddOpen( !isAddOpen )
  }

  return (
    <>

      { !isClipOpen && !isDirCreateOpen && !isAddOpen ? (
        <footer>

          <ClipInfo toggle={ toggleClip }>
            <FiClipboard />
          </ClipInfo>

          <button onClick={ toggleAdd }>
            <FiPlus />
          </button>

          <button onClick={ toggleDirCreate }>
            <FiFolder />
          </button>

        </footer>
      ) : null }

      { isClipOpen ? (
        <Clip toggle={ toggleClip } isOpen={ isClipOpen } />
      ) : null }

      { isDirCreateOpen ? (
        <DirCreate toggle={ toggleDirCreate } />
      ) : null }

      { isAddOpen ? (
        <DirAdd toggle={ toggleAdd } />
      ) : null }

    </>
  )
}

export default ToolBar
