import React from 'react';
import ReactDOM from 'react-dom';

import '@ipui/skin/index.css'
import '@ipui/skin/theme/moon.css'

import IpuiStorage from './container/ipuiStorage/IpuiStorage'

ReactDOM.render(<IpuiStorage />, document.getElementById('root'));

